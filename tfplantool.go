package main

import (
	"fmt"
	"os"

	"github.com/davecgh/go-spew/spew"
	"github.com/urfave/cli/v2"
	"gitlab.com/sbmagar/tfplantool/plancomponents"
)

func main() {
	app := &cli.App{
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     "file",
				Aliases:  []string{"f"},
				Required: true,
			},
		},
		Commands: []*cli.Command{
			{
				Name:    "dump",
				Aliases: []string{"d"},
				Usage:   "dump the plan",
				Action:  dump,
			},
			{
				Name:    "backend",
				Aliases: []string{"b"},
				Usage:   "backend configuration",
				Subcommands: []*cli.Command{
					{
						Name:   "dump",
						Usage:  "dump the backend configuration",
						Action: backendDump,
					},
					{
						Name:   "get",
						Usage:  "get the value of a key from the backend configuration",
						Action: backendGet,
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:     "key",
								Aliases:  []string{"k"},
								Required: true,
							},
						},
					},
					{
						Name:   "set",
						Usage:  "set the value of a key in the backend configuration",
						Action: backendSet,
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:     "key",
								Aliases:  []string{"k"},
								Required: true,
							},
							&cli.StringFlag{
								Name:     "value",
								Aliases:  []string{"v"},
								Required: true,
							},
						},
					},
				},
			},
		},
	}

	app.Run(os.Args)
}

func dump(c *cli.Context) error {
	pc, err := plancomponents.FromFile(c.String("file"))
	if err != nil {
		return cli.Exit(err, 1)
	}

	spew.Dump(pc)

	return nil
}

func backendDump(c *cli.Context) error {
	pc, err := plancomponents.FromFile(c.String("file"))
	if err != nil {
		return cli.Exit(err, 1)
	}

	spew.Dump(pc.Plan.Backend.Config)

	return nil
}

func backendGet(c *cli.Context) error {
	pc, err := plancomponents.FromFile(c.String("file"))
	if err != nil {
		return cli.Exit(err, 1)
	}

	value, err := pc.Get(c.String("key"))
	if err != nil {
		return cli.Exit(err, 1)
	}

	if value != "" {
		fmt.Println(value)
	}

	return nil
}

func backendSet(c *cli.Context) error {
	pc, err := plancomponents.FromFile(c.String("file"))
	if err != nil {
		return cli.Exit(err, 1)
	}

	err = pc.Set(c.String("key"), c.String("value"))
	if err != nil {
		return cli.Exit(fmt.Errorf("failed to set %s: %w", c.String("key"), err), 1)
	}

	err = pc.Write(c.String("file"))
	if err != nil {
		return cli.Exit(fmt.Errorf("failed to write plan file: %w", err), 1)
	}

	return nil
}

